﻿using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using System.Collections.Generic;

namespace App4
{
    // Define your own ContentDialogResult enum
    public enum Results
    {
        Speichern,
        Abbrechen,
        xyz,
        abc
    }

    

    public sealed partial class CreateCustomerPage : ContentDialog
    {
        public Results Result { get; set; }

        public string kunde { get; set; }
        public string kundennummer { get; set; }
        

        public CreateCustomerPage()
        {
            this.InitializeComponent();
            this.Result = Results.abc;
        }

        // Handle the button clicks from dialog
        private void btn_createCustomer_Click(object sender, RoutedEventArgs e)
        {
            this.Result = Results.Speichern;
            this.kunde = txt_createCustomerName.Text;
            this.kundennummer = txt_createCustomerNumber.Text;           


            // Close the dialog
            dialog.Hide();
        }

        private void btn_cancelCustomer_Click(object sender, RoutedEventArgs e)
        {
            this.Result = Results.Abbrechen;
            // Close the dialog
            dialog.Hide();
        }

        private void btn_xyzCustomer_Click(object sender, RoutedEventArgs e)
        {
            this.Result = Results.xyz;
            // Close the dialog
            dialog.Hide();
        }

       
    }
}