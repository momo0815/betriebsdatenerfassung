﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App4
{
    public abstract class TableEntry //Basisklasse für alle Tabelleneinträge
    {
        // Klassenvariablen, diese Eigenschaften sind für alle Instanzen der Klasse  gleich (auch für abgeleitete Klassen (Customer, Article...), deswegen muss dort das Schlüsselwort new eingefügt werden
        public static  int runningIndex;
        public static readonly String[] columnHeaders;
        public static readonly String tableName;

        public enum UPDATESTATUS { NEW, UNSYNCED, OLD }; //Welchen Status kann ein Tabelleneintrag haben??


        // Instanzvariablen, diese Eigenschaften sind individuell für jede Instanz der Klasse TableEntry (und abeleitete Klassen, wie Customer, Article...)
        public int updateStatus;
        public String id;


        public String createInsertionCommand(List<String> insertionValues, String tableName, String[] columnHeaders)
        {
            String mySQLinsertionString = "INSERT INTO " + tableName + " (";
            String values = "(";
            

            for (int i = 1; i < columnHeaders.Length-1; i++) //Bei 1 starten, damit ID nicht mitgeschrieben wird
            {
                mySQLinsertionString += columnHeaders[i];
                values += "\"" + insertionValues.ElementAt(i-1) + "\"";

                mySQLinsertionString += ",";
                values += ",";                
            }
            mySQLinsertionString += columnHeaders[columnHeaders.Length - 1];
            values += "\"" + insertionValues.ElementAt(insertionValues.Count-1) + "\"";

            mySQLinsertionString += ") VALUES ";
            mySQLinsertionString += values;
            mySQLinsertionString += ");";

            

            return mySQLinsertionString;
        }
        
            
        
    }



    
}
