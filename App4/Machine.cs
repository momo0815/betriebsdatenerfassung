﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App4
{
    public class Machine : TableEntry
    {
        // Klassenvariablen, diese Eigenschaften sind für alle Instanzen der Klasse Machine gleich! (wegen static)
        new public static int runningIndex = 0;
        new public static readonly String[] columnHeaders = { "ID", "Baujahr", "Hersteller", "Maschinenname", "Schliesskraft", "Zaehler" };
        new public static readonly String tableName = "Maschinen";



        // Instanzvariablen, diese Eigenschaften sind individuell für jede Instanz der Klasse Article


        public String baujahr;
        public String hersteller;
        public String maschinenname;
        public String schliesskraft;
        public String zaehler;





        public Machine(String baujahr, String hersteller, String maschinenname, String schliesskraft, String zaehler)
        {
            runningIndex++; //Beim Anleegn eines Eintrags wird der Index um 1 erhöht.
            this.id = runningIndex.ToString();
            this.baujahr = baujahr;
            this.hersteller = hersteller;
            this.maschinenname = maschinenname;
            this.schliesskraft = schliesskraft;
            this.zaehler = zaehler;
            this.updateStatus = (int)UPDATESTATUS.NEW;
        }
    }
}
