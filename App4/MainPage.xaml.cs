﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using MySql.Data;
using MySql.Data.MySqlClient;
using Windows.UI.Popups;
using Windows.Devices.SerialCommunication;
using Windows.Storage.Streams;
using System.Collections.ObjectModel;
using System.Threading;
using Windows.Devices.Enumeration;
using System.Threading.Tasks;


// Die Elementvorlage "Leere Seite" wird unter https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x407 dokumentiert.

namespace App4
{
    /// <summary>
    /// Eine leere Seite, die eigenständig verwendet oder zu der innerhalb eines Rahmens navigiert werden kann.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        private MySqlConnection conn;

        private SerialDevice serialPort = null;
        DataReader dataReaderObject = null;
        private ObservableCollection<DeviceInformation> listOfDevices;
        private CancellationTokenSource ReadCancellationTokenSource;




        public static MainPage current;



        public MainPage()
        {
            this.InitializeComponent();
            current = this;
            listOfDevices = new ObservableCollection<DeviceInformation>();
            ListAvailablePorts();

        }

        private async void ListAvailablePorts()
        {
            try
            {
                string aqs = SerialDevice.GetDeviceSelector();
                var dis = await DeviceInformation.FindAllAsync(aqs);

                txt_Serial_Status.Text = "Select a device and connect";
                

                for (int i = 0; i < dis.Count; i++)
                {
                    var port = await SerialDevice.FromIdAsync(dis[i].Id);
                    if (port!=null)
                    {
                        listOfDevices.Add(dis[i]);
                        cmb_Serial.Items.Add(port.PortName);
                        port.Dispose();
                    }                          
                }

               // cmb_Serial.ItemsSource = listOfDevices;
               
                cmb_Serial.SelectedIndex = -1;
            }
            catch (Exception ex)
            {
                txt_Serial_Status.Text = "List Ports: " + ex.Message;
            }
        }





        private void btnConnect_Click(object sender, RoutedEventArgs e)
        {
            Login(txtServer.Text,
                            txtPort.Text,
                            txtDBName.Text,
                            txtUser.Text,
                            txtPasswort.Password);
        }

        public async void Login(string server, string port, string dbname, string user, string password)
        {
            string myConnectionString = "Server=" + server + ";" +
                                        "Port=" + port + ";" +
                                        "Database=" + dbname + ";" +
                                        "Uid=" + user + ";" +
                                        "Pwd=" + password + ";" +
                                        "SslMode=None" + ";" +
                                        "Charset=utf8";
            conn = new MySql.Data.MySqlClient.MySqlConnection();
            conn.ConnectionString = myConnectionString;

            try
            {
                conn.Open();
                conn.Close();
            }
            catch (MySql.Data.MySqlClient.MySqlException ex)
            {
                var SqlException = new MessageDialog(ex.Message);
                await SqlException.ShowAsync();
                return;
            }
            catch (Exception ex)
            {
                var UnkownException = new MessageDialog(ex.Message);
                await UnkownException.ShowAsync();
                return;
            }
            var Success = new MessageDialog("Verbindung hergestellt");
            await Success.ShowAsync();
            return;
        }

        private async void initData()
        {


            // Kunden einlesen
            String kunde = "";
            String kundennummer = "";

            String sql = " SELECT * FROM " + Customer.tableName + ";";

             conn.Open();
            MySqlCommand cmd = new MySqlCommand(sql, conn);

            //HIER PRÜFEN, OB VERBINDUNG VORHANDEN ETC.
            MySqlDataReader reader =  cmd.ExecuteReader();

            while (reader.Read())
            {
                kunde = reader.GetString("Kunde");
                kundennummer = reader.GetString("Kundennummer");

                Customer customer = new Customer(kunde, kundennummer);
                DataHandler.kundenListe.Add(customer);
            }
            conn.Close();
            // Artikel einlesen
            conn.Open();
            String artikel = "";
            String artikelnummer = "";
            String gewicht = "";
            String kavitaeten = "";
            String material = "";
            

            sql = " SELECT * FROM " + Article.tableName + ";";
            cmd = new MySqlCommand(sql, conn);

            //HIER PRÜFEN, OB VERBINDUNG VORHANDEN ETC.
            reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                artikel = reader.GetString("Artikelbezeichnung");
                artikelnummer = reader.GetString("Artikelnummer");
                gewicht = reader.GetString("Artikelnummer");
                kavitaeten = reader.GetString("Artikelnummer");
                material = reader.GetString("Artikelnummer");
                kundennummer = reader.GetString("Kundennummer");

                Article article = new Article(artikel,artikelnummer,gewicht,kavitaeten,material,kundennummer);
                DataHandler.artikelListe.Add(article);
                
            }
            conn.Close();
        }

        private async void connectToCOMPort()
        {
            String port = cmb_Serial.SelectedItem.ToString();

            var aqsFilter = SerialDevice.GetDeviceSelector(port);
            try
            {
                var devices = await DeviceInformation.FindAllAsync(aqsFilter);
                if (devices.Any())
                {
                    var deviceId = devices.First().Id;
                    this.serialPort = await SerialDevice.FromIdAsync(deviceId);

                    if (this.serialPort != null)
                    {
                        this.serialPort.BaudRate = 38400;
                        this.serialPort.StopBits = SerialStopBitCount.One;
                        this.serialPort.DataBits = 8;
                        this.serialPort.Parity = SerialParity.None;
                        this.serialPort.Handshake = SerialHandshake.None;
                        this.serialPort.WriteTimeout = TimeSpan.FromMilliseconds(2000);
                        this.serialPort.ReadTimeout = TimeSpan.FromMilliseconds(2000);

                        this.dataReaderObject = new DataReader(this.serialPort.InputStream);
                        txt_Serial_Status.Text = "Serial port configured successfully: ";

                        ReadCancellationTokenSource = new CancellationTokenSource();
                        Listen();
                    }
                }
            }
            catch (Exception ex)
            {
                txt_Serial_Status.Text = "Connect to ComPort" + ex.Message;
            }
            
        }

        

        private async void Listen()
        {
            try
            {
                if (serialPort != null)
                {
                    dataReaderObject = new DataReader(serialPort.InputStream);

                    // keep reading the serial input
                    while (true)
                    {
                        await ReadAsync(ReadCancellationTokenSource.Token);
                    }
                }
            }
            catch (TaskCanceledException tce)
            {
                txt_Serial_Status.Text  = "LISTEN: Reading task was cancelled, closing device and cleaning up";
                CloseDevice();
            }
            catch (Exception ex)
            {
                txt_Serial_Status.Text = "LISTEN: " + ex.Message;
            }
            finally
            {
                // Cleanup once complete
                if (dataReaderObject != null)
                {
                    dataReaderObject.DetachStream();
                    dataReaderObject = null;
                }
            }
        }

        private async Task ReadAsync(CancellationToken cancellationToken)
        {
            Task<UInt32> loadAsyncTask;

            uint ReadBufferLength = 1024;
           
            cancellationToken.ThrowIfCancellationRequested();
           
            dataReaderObject.InputStreamOptions = InputStreamOptions.Partial;

            using (var childCancellationTokenSource = CancellationTokenSource.CreateLinkedTokenSource(cancellationToken))
            {               
                loadAsyncTask = dataReaderObject.LoadAsync(ReadBufferLength).AsTask(childCancellationTokenSource.Token);
             
                UInt32 bytesRead = await loadAsyncTask;
                if (bytesRead > 0)
                {
                    txt_Serial_RX.Text = dataReaderObject.ReadString(bytesRead);
                    txt_Serial_Status.Text = "Hurensohn!";
                }
            }
        }

        /// <summary>
        /// CancelReadTask:
        /// - Uses the ReadCancellationTokenSource to cancel read operations
        /// </summary>
        private void CancelReadTask()
        {
            if (ReadCancellationTokenSource != null)
            {
                if (!ReadCancellationTokenSource.IsCancellationRequested)
                {
                    ReadCancellationTokenSource.Cancel();
                }
            }
        }

        /// <summary>
        /// CloseDevice:
        /// - Disposes SerialDevice object
        /// - Clears the enumerated device Id list
        /// </summary>
        private void CloseDevice()
        {
            if (serialPort != null)
            {
                serialPort.Dispose();
            }
            serialPort = null;

           
            txt_Serial_RX.Text = "";
            listOfDevices.Clear();
        }

        private async void btnKundeAnl_Click(object sender, RoutedEventArgs e)
        {
            
            CreateCustomerPage dialog = new CreateCustomerPage();
            await dialog.ShowAsync();

          
            if (dialog.Result == Results.Speichern)
            {
                Customer newCustomer = new Customer(dialog.kunde, dialog.kundennummer);

                DataHandler.kundenListe.Add(newCustomer);
                List<String> insertionValues = new List<String>();
                insertionValues.Add(dialog.kunde);
                insertionValues.Add(dialog.kundennummer);

                String mySqlInsertionCommand = newCustomer.createInsertionCommand(insertionValues,Customer.tableName, Customer.columnHeaders);

                conn.Open();

                using (MySqlCommand cmd = new MySqlCommand(mySqlInsertionCommand, conn))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.ExecuteNonQuery();
                }
                conn.Close();
            }
            else if (dialog.Result == Results.Abbrechen)
            {

            }
            else if (dialog.Result == Results.xyz)
            {

            }
        }

        private async void btnArtikelAnl_Click(object sender, RoutedEventArgs e)
        {
            CreateArticlePage dialog = new CreateArticlePage();
            await dialog.ShowAsync();


            if (dialog.Result == Results.Speichern)
            {
                Article newArticle = new Article(dialog.artikel, dialog.artikelnummer, dialog.gewicht, dialog.kavitaeten, dialog.material, dialog.kundennummer);


                DataHandler.artikelListe.Add(newArticle);
                List<String> insertionValues = new List<String>();
                insertionValues.Add(dialog.artikel);
                insertionValues.Add(dialog.artikelnummer);
                insertionValues.Add(dialog.gewicht);
                insertionValues.Add(dialog.kavitaeten);
                insertionValues.Add(dialog.material);
                insertionValues.Add(dialog.kundennummer);

                String mySqlInsertionCommand = newArticle.createInsertionCommand(insertionValues, Article.tableName, Article.columnHeaders);

                conn.Open();
                using (MySqlCommand cmd = new MySqlCommand(mySqlInsertionCommand, conn))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.ExecuteNonQuery();
                }
                conn.Close();
            }
            else if (dialog.Result == Results.Abbrechen)
            {

            }
            else if (dialog.Result == Results.xyz)
            {

            }

        }

        private void btnInit_Click(object sender, RoutedEventArgs e)
        {
            initData();
        }

        private void btn_Serial_Connect_Click(object sender, RoutedEventArgs e)
        {
            connectToCOMPort();
        }

        private async void btnFertigungsauftragAnl_Click(object sender, RoutedEventArgs e)
        {
            CreateManufacturingOrderPage dialog = new CreateManufacturingOrderPage();
            await dialog.ShowAsync();


            if (dialog.Result == Results.Speichern)
            {
                
            }
            else if (dialog.Result == Results.Abbrechen)
            {

            }
            else if (dialog.Result == Results.xyz)
            {

            }
        }
    }
}
