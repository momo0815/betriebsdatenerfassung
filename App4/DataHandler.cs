﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App4
{
    public static class DataHandler //statische Klasee, damit alle anderen Klassen unabhängig von einer existierenden Instanz darauf zugreifen können.
    {
        public static List<Customer> kundenListe { get; set; } = new List<Customer>();
        public static List<Article> artikelListe { get; set; } = new List<Article>();
        public static List<Machine> maschinenListe { get; set; } = new List<Machine>();







        public static bool readCustomerFromID(String id, String dest) //Durchsucht das Array kunden nach dem Kunden mit der entsprechenden ID und schreibt in den string dest dann dessen Kundennummer und Kundenname
        {
            String str;


            for (int i = 0; i < kundenListe.Count; i++)
            {
                if (kundenListe.ElementAt(i).id.Equals(id))
                {
                    str = kundenListe.ElementAt(i).kundennummer + " " + kundenListe.ElementAt(i).kunde;
                    dest = new String(str.ToCharArray());
                    return true;
                }
            }
            return false;
        }

    }


}
