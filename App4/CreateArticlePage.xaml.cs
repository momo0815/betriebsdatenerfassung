﻿
using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;


namespace App4
{  

    public sealed partial class CreateArticlePage : ContentDialog
    {
        public Results Result { get; set; }

        public string artikel { get; set; }
        public string artikelnummer { get; set; }
        public string gewicht { get; set; }
        public string kavitaeten { get; set; }
        public string material { get; set; }
        public string kundennummer { get; set; }

        public CreateArticlePage()
        {
            this.InitializeComponent();

            this.initCustomerComboBox();




            this.Result = Results.abc;
        }

        // Handle the button clicks from dialog
        private void btn_createArticle_Click(object sender, RoutedEventArgs e)
        {
            this.Result = Results.Speichern;

            this.artikel = txt_createArticleName.Text;
            this.artikelnummer = txt_createArticleNumber.Text;
            this.gewicht = txt_createArticleWeight.Text;
            this.kavitaeten = txt_createArticleCavities.Text;
            this.material = txt_createArticleMaterial.Text;
            this.kundennummer = cmb_customer.SelectedItem.ToString();


            // Close the dialog
            articleDialog.Hide();
        }

        private void btn_cancelArticle_Click(object sender, RoutedEventArgs e)
        {
            this.Result = Results.Abbrechen;
            // Close the dialog
            articleDialog.Hide();
        }

        private void btn_xyzArticle_Click(object sender, RoutedEventArgs e)
        {
            this.Result = Results.xyz;
            // Close the dialog
            articleDialog.Hide();
        }


        private void initCustomerComboBox()
        {
            
            foreach (Customer customer in DataHandler.kundenListe)
            { 
                
                cmb_customer.Items.Add(customer);

            }
        }

    }
}