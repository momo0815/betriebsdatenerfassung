﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace App4
{
    public class Customer : TableEntry
    {
        // Klassenvariablen, diese Eigenschaften sind für alle Instanzen der Klasse Customer gleich!   (wegen static)
        new public static int runningIndex = 0;
        new public static readonly String[] columnHeaders = { "ID", "Kunde", "Kundennummer" };
        new public static readonly String tableName = "Kunden";



        // Instanzvariablen, diese Eigenschaften sind individuell für jede Instanz der Klasse Customer            


        public String kunde;
        public String kundennummer;

        //public Article[] articles;   // es macht mMn Sinn, der Klasse Customer ein Array mit Articles zuzuweisen. In der DB ist der Bezug anders realisiert, das bleibt davon unberührt.


        public Customer(String kunde, String kundennummer)
        {


            runningIndex++; //Beim Anleegn eines Eintrags wird der Index um 1 erhöht.
            this.id = runningIndex.ToString();
            this.kunde = kunde;
            this.kundennummer = kundennummer;
            this.updateStatus = (int) UPDATESTATUS.NEW;
        }

        public override string ToString()
        {
            return this.kundennummer + " " + this.kunde;
            
        }


    }
}
