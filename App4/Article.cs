﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App4
{
    public class Article : TableEntry
    {
        // Klassenvariablen, diese Eigenschaften sind für alle Instanzen der Klasse Article gleich! (wegen static)
        new public static int runningIndex = 0;
        new public static readonly String[] columnHeaders = { "ID", "Artikelbezeichnung", "Artikelnummer", "Gewicht", "Kavitaeten", "Material","Kundennummer"};
        new public static readonly String tableName = "Artikel";



        // Instanzvariablen, diese Eigenschaften sind individuell für jede Instanz der Klasse Article


        public String artikelBezeichnung;
        public String artikelNummer;
        public String gewicht;
        public String kavitaeten;
        public String material;
        public String kundennummer; 

       


        public Article(String artikelbezeichnung, String artikelnummer, String gewicht, String kavitaeten, String material, String kundennummer)
        {
            runningIndex++; //Beim Anleegn eines Eintrags wird der Index um 1 erhöht.
            this.id = runningIndex.ToString();
            this.artikelBezeichnung = artikelbezeichnung;
            this.artikelNummer = artikelnummer;
            this.gewicht = gewicht;
            this.kavitaeten = kavitaeten;
            this.material = material;
            this.kundennummer = kundennummer;
            this.updateStatus = (int)UPDATESTATUS.NEW;
            
        }

        





    }
}
