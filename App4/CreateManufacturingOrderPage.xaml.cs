﻿
using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using System.Collections.Generic;


namespace App4
{

    public sealed partial class CreateManufacturingOrderPage : ContentDialog
    {
        public Results Result { get; set; }

        public CreateManufacturingOrderPage()
        {
            this.InitializeComponent();
            this.Result = Results.abc;
        }

        private void btn_createManufacturingOrder_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btn_cancelManufacturingOrder_Click(object sender, RoutedEventArgs e)
        {
            this.Result = Results.Abbrechen;
            // Close the dialog
            //dialog.Hide();
        }
    }
}